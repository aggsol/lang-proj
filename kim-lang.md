# PROJECT KIM-LANG

This is a work in progress specification for a yet unnamed scripting language.

## Goals
* Explicit and verbose syntax
* Embeddable in C++
* Break classic expectations
* Force naming of types, methods, procedures
* Variable are of the type assigned first and enforce it.
* No `;`
* No `{}`
* No `[]`
* No ternary operator
* Exceptions? No.
* Coroutines? Threads? No.
* Modules? No.
* No operator overloading

## Built Ins
* List
    - sparse?
* echo? or as procedure?

## Standard Lib?
* JSON
* include/import
* io?
* GUI?

## Language

* Literals. Numbers, strings, Booleans, and `nil`.
* Unary expressions. A operator `not` to perform a logical not, and `-` to negate a number.
* Binary expressions. 
    - The infix arithmetic (+, -, *, /) and comparison (==, !=, <, <=, >, >=) operators.
    - Boolean operators `and` `or` 
* Parentheses for grouping.

* Procedures are also values (Closures)
* Empty type `Type`?

Does `-12.34` produce on or two tokens?

### Tokens

* Left, Right Round Brackets
* Comma, Dot
* Plus, Minus, Slash, Asterisk
* Hash

* Bang Equal,
* Equal, Equal Equal
* Greater, Greater Equal,
* Less, Less Equal
* Plus Equal, Minus Equal, Slash Equal, Star Equal

* Identifier, String, Number
* Newline


#### Keywords

* And, Or, Not, True, False
* Nil, Procedure, Return
* Type, Self, Extends, Mother
* For, Break, Continue
* If, Then, Else, 
* While, Do,
* Var, End

* Echo, Import, Exit


## Example

```
#!/usr/bin/env kim

import somefile.kim
import io

echo("Hello World!")

var half = (12.45 + 2) / 1.22
half *= 0.5

# half = "abc" is an error. Variables cannot change type once set

# procedures can return a value like a function
procedure calcSum(a, b)
    return a + b
end

procedure echoSum(a, b)
    echo(calcSum(a, b))
    return # return nothing or nil?
end

var a = calcSum(half, 1.23)

var message
if(a > 10) then
    message = "is larger than 10"
    if(a > 100) then
        message = "is larger than 100"
    end
else 
    if(half == 10) then
        message = "is 10"
    else
        message = "is smaller than 10"
    end
end

echo(message)

procedure outerFunction() 
  procedure localFunction() 
    echo("I'm local!")
  end

  localFunction()
end

# declaration of custom types
type Foo
    initialize(data) # is called automagically when the type is instanciated
        self.data = data
    end
    
    flyTo(city) # define member procedures
        echo("fly to " + city)
    end
end

# Bar is a type that extends the mother type Foo
type Bar extends Foo 
    initialize(data, other)
        mother.initialize(data) # mother type access automagically on initialize?
        self.other = other # self refers to the current instance
        
        mother.field = "bookworm"
    end
end

var bar = Bar("Bar", 123)
bar.other = 12
bar.more = "more data"
bar.more = nil

var flag = false
while not flag do
    flag = true
end

for var i=1, i<10, i=i+1 do
    echo i
end

var data = List()
data.append("asd")
data.append(12)
data.append(bar)
data.name = "misc"

data.push(Foo("Hamburg"))
var old = data.pop();

echo(data.at(0))
data.at(1) += 12

for i=0, i<=data.length(), i=i+1  do
    var value = data.at(i)
    if(value == 12) then
        break
    end
    echo(value);
end

data.clear()
data.length()
data.removeAt()
data.removeLast()
```

## Spec

* Case sensentive
* UTF-8 encoded without BOM or ASCII-only?
* NUL character (U+0000) is not allowed
* White space, formed from spaces (U+0020), horizontal tabs (U+0009), carriage returns (U+000D), and newlines (U+000A)
* No semicolons `;` as terminators

### Comments
The hash symbol `#` marks the rest of the line as comment
```
# comment line
var i = 3 # rest of the line comment
```

### Identifiers
Identifiers name type, variable, procedures.

```
IDENTIFIER = letter (letter | digit)+ ;
```

### Strings

* Strings must contain valid UTF-8 characters
* Surrounded by quotation marks
* 

### Number Literals
Represented as float64 (double)
```
NUMBER  = integer 
        | float ;
integer = ("-")? digit (digit)* ;
float   = ("-")? (digit)+ "." (digit)+ ;
```

```
var a = 1
var b = -1
var c = 1.234
var d = -1.234
```

### Booleans
```
true
false
```

### Variables

### Procedures

### Types


## C++ Extension API
```cpp
Runtime runtime;
runtime.addVar(..., "varName");
funtime.addProc(..., "procedureName")
```
