/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_EXPRESSION_HPP
#define BODHI_EXPRESSION_HPP

#include "Token.hpp"
#include "Value.hpp"

#include <memory>

namespace bodhi
{
    class Binary;
    class Unary;
    class Literal;
    class Grouping;
    
    class IExpressionVisitor
    {
    public:
        virtual void visitBinary(Binary& expr) = 0;
        virtual void visitUnary(Unary& expr) = 0;
        virtual void visitLiteral(Literal& expr) = 0;
        virtual void visitGrouping(Grouping& expr) = 0;
        
    protected:
        virtual ~IExpressionVisitor() {}
    };
    
    class Expression
    {
    public:
        typedef std::shared_ptr<Expression> Ptr;
    
        virtual void accept(IExpressionVisitor& visitor) = 0;
    
    protected:
        virtual ~Expression() {}
    };
    
    class Binary : public Expression
    {
    public:
        Binary(Expression::Ptr left, Token::Ptr token, Expression::Ptr right)
        : m_left(left)
        , m_operator(token)
        , m_right(right)
        {}

        ~Binary() override {}
    
        void accept(IExpressionVisitor& visitor) override
        {
            visitor.visitBinary(*this);
        }
        
        Expression::Ptr     m_left;
        Token::Ptr          m_operator;
        Expression::Ptr     m_right;
    };

    class Grouping : public Expression
    {
    public:

        explicit Grouping(Expression::Ptr expr)
        : m_expression(expr)
        {}

        ~Grouping() override {}
    
        void accept(IExpressionVisitor& visitor) override
        {
            visitor.visitGrouping(*this);
        }
        
        Expression::Ptr   m_expression;
    };

    class Unary : public Expression
    {
    public:
        Unary(Token::Ptr token, Expression::Ptr right)
        : m_operator(token)
        , m_right(right)
        {}

        ~Unary() override {}
    
        void accept(IExpressionVisitor& visitor) override
        {
            visitor.visitUnary(*this);
        }
        
        Token::Ptr          m_operator;
        Expression::Ptr     m_right;
    };
    
    class Literal : public Expression
    {
    public:
        
        explicit Literal(const Value& val)
        : m_value(val)
        {}
        
        Literal()
        : m_value()
        {}
        
        explicit Literal(const char* str)
        : Literal(std::string(str))
        {}
        
        explicit Literal(bool value)
        : m_value(value)
        {}

        explicit Literal(double value)
        : m_value(value)
        {}
        
        explicit Literal(const std::string& value)
        : m_value(value)
        {}

        void accept(IExpressionVisitor& visitor) override
        {
            visitor.visitLiteral(*this);
        }
        
        ~Literal() override {}
    
        const Value& value() const { return m_value; };

    private:
        Value   m_value;

    };
}

#endif