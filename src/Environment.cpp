#include "Environment.hpp"

namespace bodhi
{
    Environment::Environment()
    : Environment(nullptr)
    {}
    
    Environment::Environment(Environment::Ptr enclosing)
    : m_values()
    , m_enclosing(enclosing)
    {
        
    }
}