/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "Keyword.hpp"

namespace bodhi
{
    namespace
    {
        std::array<Keyword, 19> s_keywords =  {{
             { "true",      TokenType::True }
            ,{ "false",     TokenType::False}
            ,{ "and",       TokenType::And}
            ,{ "or",        TokenType::Or}
            ,{ "not",       TokenType::Not}
            ,{ "nil",       TokenType::Nil}
            ,{ "procedure", TokenType::Procedure}
            ,{ "return",    TokenType::Return}
            ,{ "type",      TokenType::Type}
            ,{ "self",      TokenType::Self}
            ,{ "extends",   TokenType::Extends}
            ,{ "mother",    TokenType::Mother}
            ,{ "for",       TokenType::For}
            ,{ "to",        TokenType::To}
            ,{ "step",      TokenType::Step}
            ,{ "if",        TokenType::If}
            ,{ "while",     TokenType::While}
            ,{ "end",       TokenType::End}
            ,{ "var",       TokenType::Variable}
        }};
    }
    
    const std::array<Keyword, 19>& Keyword::getKeywords()
    {
        return s_keywords;
    }
}