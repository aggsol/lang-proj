/**
* Command Line Argument
* Adhering the POSIX and GNU command line parameter standards.
*
* POSIX:
* Options are a hyphen followed by a single alphanumeric character: '-o'
* Options may require an argument: '-o argument' or '-oargument'
* Options without arguments are of type bool
* Options without arguments can be grouped after a hyphen: '-lst' same as '-t -l -s'
* Options can appear in any order: '-lst' same as '-tls'
* The '--' argument terminates options
*
*   bodhi::CliArg<std::string> inputFile(argc, argv, "i", "input");
*
* GNU:
* Define long name options: '--output' for '-o'
*
* Always provide '-h', '--help' and '--version' options.
*
*   bodhi::CliArg< bool > help(argc, argv, "h", "help");
*   bodhi::CliArg< bool > version(argc, argv, "", "version");
*
*   if( help ) // show usage
*   if( version ) // show version
*
* Version: 1.1.0
* ----------------------------------------------------------------------------
* "THE BEER-WARE LICENSE" (Revision 42):
* <foss@aggsol.de> wrote this file. As long as you retain this
* notice you can do whatever you want with this stuff. If we meet some day,
* and you think this stuff is worth it, you can buy me a beer in return KIM
* ----------------------------------------------------------------------------
*/

#ifndef BODHI_CLI_ARG_HPP
#define BODHI_CLI_ARG_HPP

#include <string>
#include <sstream>
#include <stdexcept>
#include <cstring>
#include <cassert>

namespace bodhi
{
    /**
    * Basic functions shared by general template and specialisations
    */
    template <typename T>
    class CliArgMixin
    {
    protected:
        /**
        * Helper function to delete arguments.
        */
        static void deleteArg(int& argc, char* argv[], int pos)
        {
            assert(argv);
            assert(argc > 0);
        
            if(pos >= argc) return;

            for(int i=pos; i<argc-1; ++i)
            {
                argv[i] = argv[i+1];
            }

            argv[argc] = 0;
            argc--;
            argv[argc] = 0;
        }

        /**
        * Search by an alternative argument name.
        * Use this to use long and short arguments e.g. '-l 10' and '--list 10'
        */
        bool search(int& argc, char* argv[], const std::string& name)
        {
            assert(argv);
            assert(argc > 0);
            
            if(name.empty()) return false;

            std::string entry;
            for(int i=0; i<argc; ++i)
            {
                assert(argv[i]);
                if(argv[i][0] == '-' && argv[i][1] != '-')
                {
                    entry.assign(argv[i] + 1);
                }
                else if(argv[i][0] == '-' && argv[i][1] == '-')
                {
                    // skip on --
                    if(argv[i][2] == 0) return false;
                    entry.assign(argv[i] + 2);
                }
                else
                {
                    continue;
                }

                if(entry == name)
                {
                    extract(argc, argv, i);
                    m_extracted = true;
                    return true;
                }
            }
            return false;
        }

        /**
        * Search condesed POSIX short options like '-x12'
        */
        void searchCondensed(int& argc, char* argv[], const std::string& name)
        {
            assert(argc > 0);
            assert(argv);
            if(name.empty()) return;

            std::string entry;
            for(int i=0; i<argc; ++i)
            {
                if(argv[i][0] == '-' && argv[i][1] != '-')
                {
                    entry = argv[i][1];
                }
                else if(argv[i][0] == '-' && argv[i][1] == '-')
                {
                    // skip on --
                    if(argv[i][2] == 0) return;
                }
                else
                {
                    continue;
                }

                if(entry == name)
                {
                    extractCondensed(argc, argv, i);
                    m_extracted = true;
                    return;
                }
            }
        }

        /**
        * Returns true if an argument value from the command line was extracted
        * false otherwise.
        */
        bool extractedValue() const { return m_extracted; }

        CliArgMixin(const T& dflt)
        : m_value(dflt)
        , m_extracted(false)
        {}

        virtual ~CliArgMixin() {}

        virtual void extract(int& argc, char* argv[], int index) = 0;
        virtual void extractCondensed(int& argc, char* argv[], int index) = 0;

        T       m_value;
        bool    m_extracted;
    };

    /**
    * General template.
    */
    template <typename T>
    class CliArg : public CliArgMixin<T>
    {
        typedef CliArgMixin<T> Mixin;
    public:
        CliArg(int& argc, char* argv[], const std::string& opt, const std::string& longOpt, const T& d = T())
        : Mixin(d)
        {
            if(opt.length() > 1)
            {
                throw std::invalid_argument("Short option must be one character or empty");
            }

            if(longOpt.length() == 1)
            {
                throw std::invalid_argument("Long option must not be one character");
            }

            if( Mixin::search(argc, argv, longOpt) == false )
            {
                if( Mixin::search(argc, argv, opt) == false )
                {
                    Mixin::searchCondensed(argc, argv, opt);
                }
            }
        }

        ~CliArg() {}

        const T& value() const { return CliArgMixin<T>::m_value; }
        operator const T&() const { return CliArgMixin<T>::m_value; }

    private:
        CliArg() = delete;

        virtual void extract(int& argc, char* argv[], int index)
        {
            if(index+1 < argc)
            {
                std::istringstream(argv[index+1]) >> Mixin::m_value;
                Mixin::deleteArg(argc, argv, index);
                // TRICKY: after the i-th was a delete index is now the old i+1;
                Mixin::deleteArg(argc, argv, index);
            }
            else
            {
                std::ostringstream msg;
                msg << "Missing value for parameter: " << argv[index];
                throw std::runtime_error( msg.str() );
            }
        }

        virtual void extractCondensed(int& argc, char** argv, int index)
        {
            // skip '-' and opt
            std::istringstream(argv[index]+2) >> Mixin::m_value;
            Mixin::deleteArg(argc, argv, index);
        }
    };

    /**
    * Specialisation for std::string values
    */
    template <>
    class CliArg< std::string > : public CliArgMixin< std::string >
    {
        typedef CliArgMixin<std::string> Mixin;
    public:
        CliArg(int& argc, char* argv[], const std::string& opt, const std::string& longOpt, const std::string& d = "")
        : Mixin(d)
        {
            if(opt.length() > 1)
            {
                throw std::invalid_argument("Short option must be one character or empty");
            }

            if(longOpt.length() == 1)
            {
                throw std::invalid_argument("Long option must not be one character");
            }
            
            if( Mixin::search(argc, argv, longOpt) == false)
            {
                if( Mixin::search(argc, argv, opt) == false)
                {
                    Mixin::searchCondensed(argc, argv, opt);
                }
            }
        }

        ~CliArg() {}

        const std::string& value() const { return Mixin::m_value; }
        operator const std::string&() const  { return Mixin::m_value; }

    private:
        CliArg() = delete;

        virtual void extract(int& argc, char** argv, int index)
        {
            if(index+1 < argc)
            {
                Mixin::m_value = argv[index+1];
                Mixin::deleteArg(argc, argv, index);
                // TRICKY: after the i-th was a delete i is now the old i+1;
                Mixin::deleteArg(argc, argv, index);
            }
            else
            {
                std::ostringstream msg;
                msg << "Missing value for parameter: " << argv[index];
                throw std::runtime_error( msg.str() );
            }
        }
        virtual void extractCondensed(int& argc, char** argv, int index)
        {
            // skip '-' and opt
            Mixin::m_value = argv[index]+2;
            Mixin::deleteArg(argc, argv, index);
        }
    };

    /**
    * Specialisation for bool flags with no following value.
    */
    template <>
    class CliArg< bool > : public CliArgMixin< bool >
    {
        typedef CliArgMixin< bool > Mixin;
    public:
        CliArg(int& argc, char* argv[], const std::string& opt, const std::string& longOpt)
        : Mixin(false)
        {
            assert(argc > 0);
            assert(argv);
            if(opt.length() > 1)
            {
                throw std::invalid_argument("Short option must be one character or empty");
            }

            if(longOpt.length() == 1)
            {
                throw std::invalid_argument("Long option must not be one character");
            }
            
            if( Mixin::search(argc, argv, longOpt) == false )
            {
                if( Mixin::search(argc, argv, opt) == false )
                {
                    searchCondensed(argc, argv, opt);
                }
            }
        }

        ~CliArg() {}

        bool value() const { return Mixin::m_value; }

        operator bool() const  { return Mixin::m_value; }

    private:

        CliArg() = delete;

        virtual void extract(int& argc, char** argv, int index)
        {
            Mixin::m_value = true;
            Mixin::deleteArg(argc, argv, index);
        }

        /**
        * There are no condensed value for short options
        */
        virtual void extractCondensed(int&, char**, int) {}


        /**
        * Only condensed bool are of type '-asd'
        */
        void searchCondensed(int& argc, char* argv[], const std::string& opt)
        {
            if(opt.empty()) return;

            for(int index=0; index<argc; ++index)
            {
                const size_t len = ::strlen(argv[index]);

                if(len < 2) continue;
                if( argv[index][0] != '-' ) continue;
                if( argv[index][1] == '-' ) return;

                for(uint i=1; i<len; ++i)
                {
                    if( argv[index][i] == opt[0] )
                    {
                        Mixin::m_value = true;
                        Mixin::m_extracted = true;

                        // remove the opt from the string.
                        argv[index][i] = argv[index][len-1];
                        argv[index][len-1] = 0;

                        break;
                    }
                }
            }
        }
    };

    /**
    * Extract operator
    * Just add the value itself to the out stream.
    */
    template <typename T>
    std::ostream& operator<< (std::ostream& stream, const CliArg<T>& argument)
    {
        return stream << argument.value();
    }
}

#endif