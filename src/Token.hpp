/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_TOKEN_HPP
#define BODHI_TOKEN_HPP

#include "TokenType.hpp"

#include <memory>
#include <string>

namespace bodhi
{
    class Tokenizer;
    
    class Token
    {
        friend class Tokenizer;
    public:
        typedef std::shared_ptr<Token> Ptr;
        
        TokenType                       type() const { return m_type; }
        int                             lineNum() const { return m_lineNum; }
        int                             column() const { return m_column; };
        int                             length() const { return m_length; };
        std::string                     line() const;
        
        const std::string&              lexeme();
        
    private:
        TokenType                       m_type = TokenType::Final;
        int                             m_lineNum = 0;
        int                             m_column = 0;
        int                             m_length = 0;
        std::shared_ptr<std::string>    m_line = nullptr;
        std::string                     m_lexeme = "";

    };
}

#endif