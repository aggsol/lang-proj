/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "Tokenizer.hpp"
#include "Keyword.hpp"
#include "Reporter.hpp"

#include <cassert>
#include <cstring>
#include <iostream>

namespace
{
    bool isAlpha(char c)
    {
        return (c >= 'a' && c <= 'z') ||
               (c >= 'A' && c <= 'Z');
    }

    bool isDigit(char c)
    {
        return c >= '0' && c <= '9';
    }
    
    bool isAlphaNumeric(char c)
    {
        return isAlpha(c) || isDigit(c);
    }
}

namespace bodhi
{
    Tokenizer::Tokenizer(std::shared_ptr<std::string> line, unsigned lineNum, Reporter& reporter)
    : m_tokens()
    , m_line(line)
    , m_lineNum(lineNum)
    , m_reporter(reporter)
    {
        assert(m_line != nullptr);
        assert(line > 0);
    }
    
    bool Tokenizer::isEnd() const
    {
        return m_current >= m_line->length();
    }
    
    char Tokenizer::advance()
    {
        assert(m_line != nullptr);
        assert(m_current < m_line->length());
        
        char old = m_line->at(m_current);
        m_current++;
        return old;
    }
    
    char Tokenizer::current() const
    {
        assert(m_line != nullptr);
        if(m_current >= m_line->length())
        {
            return 0;
        }
        return m_line->at(m_current);
    }
    
    char Tokenizer::next() const
    {
        assert(m_line != nullptr);
        if(m_current < (m_line->length()-1))
        {
            assert(m_current+1 < m_line->length());
            return m_line->at(m_current+1);
        }
        return 0;
    }
    
    void Tokenizer::handleIdentifier()
    {
        
        while(not isEnd() && isAlphaNumeric(current()))
        {
            advance();
        }
        
        auto type = TokenType::Identifier;
        const auto length = m_current - m_tokenStart;
        assert(length > 0);
        
        for(auto& k : Keyword::getKeywords())
        {
            // std::cout << "k.name.length=" << k.name.length()
            //     << " name=" << k.name
            //     << " m_tokenStart=" << m_tokenStart
            //     << " length=" << length << "\n";
            if(length == k.name.length())
            {
                // std::cout << "substr=" << m_line->substr(m_tokenStart, length) << "\n";
                if(m_line->compare(m_tokenStart, length, k.name) == 0)
                {
                    type = k.type;
                    break;
                }
            }
        }
        
        appendToken(type);
    }
    
    void Tokenizer::handleNumber()
    {
        while(not isEnd() && isDigit(current()))
        {
            advance();
        }
        
        if(isAlpha(current()))
        {
            m_hasError = true;
            m_reporter.reportError(*m_line, m_lineNum, m_current, 
                "Unexpected character");
            return;
        }
        
        if(not isEnd() && current() == '.' && isDigit(next()))
        {
            advance();
            while(not isEnd() && isDigit(current()))
            {
                advance();
            }
        }
        
        appendToken(TokenType::Number);
    }
    
    void Tokenizer::handleString()
    {
        advance(); // skip opening "
        while(not isEnd() && current() != '"')
        {
            advance();
        }
        
        if(isEnd() || current() != '"') 
        {
            m_hasError = true;
            m_reporter.reportError(*m_line, m_lineNum, m_current, 
                "Unterminated string");
            return;
        }
        
        advance(); // skip closing "
        appendToken(TokenType::String);
    }
    
    void Tokenizer::appendToken(TokenType type)
    {
        auto token = std::make_shared<Token>();
        
        token->m_type = type;
        token->m_lineNum = m_lineNum;
        token->m_line = m_line;
        token->m_length = m_current - m_tokenStart;
        
        if(token->m_length <= 0)
        {
            token->m_length = 1;
        }
        
        token->m_column = m_tokenStart;
        
        // std::cout << "appendToken type=" << token->type 
        //     << " length=" << token->length
        //     << " current='" << current() << "'"
        //     << std::endl;
        
        m_tokens.push_back(token);
    }
    
    std::deque<Token::Ptr>&& Tokenizer::scanTokens(std::shared_ptr<std::string> line, unsigned lineNum)
    {
        m_line = line;
        m_lineNum = lineNum;
        m_current = 0;
        m_tokenStart = 0;
        m_tokens.clear();
        m_hasError = false;
        
        return scanTokens();
    }
    
    std::deque<Token::Ptr>&& Tokenizer::scanTokens()
    {
        m_current = 0;
        while(not isEnd())
        {
            m_tokenStart = m_current;
            char curr = current();
            
            // std::cout << "curr=" << curr << " m_tokenStart=" << m_tokenStart
            //     << "\n";
            
            switch(curr)
            {
            case '(': appendToken(TokenType::LeftBracket); advance(); break;
            case ')': appendToken(TokenType::RightBracket); advance(); break;
            case ',': appendToken(TokenType::Comma); advance(); break;
            case '.': appendToken(TokenType::Dot); advance(); break;
            case '+': appendToken(TokenType::Plus); advance(); break;
            case '-': appendToken(TokenType::Minus); advance(); break;
            case '/': appendToken(TokenType::Slash); advance(); break;
            case '*': appendToken(TokenType::Asterisk); advance(); break;
            case '#':
                    while (current() != '\n' && not isEnd()) advance();
                break;
                
            case '!':
                if(match('='))
                {
                    appendToken(TokenType::BangEqual);  
                }
                else
                {
                    m_hasError = true;
                    m_reporter.reportError(*m_line, m_lineNum, m_current, "Expecting '='");
                }
                advance();
            break;
            
            case '=':
                if(match('='))
                {
                    appendToken(TokenType::EqualEqual);
                }
                else
                {
                    appendToken(TokenType::Equal);
                }
                advance();
            break;
            case '<':
                if(match('='))
                {
                    appendToken(TokenType::LessEqual);
                }
                else
                {
                    appendToken(TokenType::Less);
                }
                advance();
            break;
            case '>':
                if(match('='))
                {
                    appendToken(TokenType::GreaterEqual);
                }
                else
                {
                    appendToken(TokenType::Greater);
                }
                advance();
            break;
            
            case '"':
                handleString();
            break;
            
            // skip whitespace
            case ' ':
            case '\r':
            case '\t':
                advance();
                break;
            case '\n':
                m_lineNum++;
                appendToken(TokenType::Newline);
                advance();
                break;
            
            default:
            
                if(isAlpha(curr))
                {
                    handleIdentifier();
                }
                else if(isDigit(curr))
                {
                    handleNumber();   
                }
                else
                {
                    m_hasError = true;
                    if(curr == 0)
                    {
                        m_reporter.reportError(*m_line, m_lineNum, m_current, "Unexpected end of line.");
                    }
                    else
                    {
                        m_reporter.reportError(*m_line, m_lineNum, m_current, "Unexpected character.");
                    }
                    advance();
                }
                break;
            }
        }
        
        appendToken(TokenType::Newline);
        
        return std::move(m_tokens);
    }
    
    bool Tokenizer::match(char c)
    {
        if(isEnd()) return false;
        if(next() != c) return false;
        
        m_current++;
        return true;
    }
}