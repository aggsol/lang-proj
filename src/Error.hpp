/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_ERROR_HPP
#define BODHI_ERROR_HPP

#include "Token.hpp"

#include <string>
#include <stdexcept>

namespace bodhi
{
    class Error : public std::runtime_error
    {
    public:
    
        Error(Token::Ptr token, const std::string& msg);
        Error(int lineNum, int col, const std::string& line, const std::string& msg);
    
        ~Error() override {}
 
        int         m_lineNum = 0;
        int         m_column = 0;
        
        std::string m_line;
        Token::Ptr  m_token;
    };
}

#endif