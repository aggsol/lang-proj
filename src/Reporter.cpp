/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/

#include "Reporter.hpp"
#include "Error.hpp"

#include <cassert>

namespace bodhi
{
    Reporter::Reporter(bool useColor)
    : Reporter(useColor, std::cout)
    {}
    
    Reporter::Reporter(bool useColor, std::ostream& strm)
    : m_output(strm)
    , m_useColor(useColor)
    , m_filename()
    {}
    
    void Reporter::reportError(const std::string& line, 
        int lineNum, int col, const std::string& msg)
    {
        if(not m_filename.empty())
        {
            m_output << m_filename << ":";         
        }
        m_output << lineNum << ":" << (col+1) << ": "
            << error() << " "
            << msg << std::endl;
        
        m_output << line << std::endl;
                    
        for(int i=0; i<col; ++i)
        {
            m_output << '.';
        }
        m_output << indicator() << std::endl;  
    }
     
    void Reporter::reportError(Token::Ptr token, const std::string& msg)
    {
        assert(token != nullptr);
        
        reportError(token->line(), token->lineNum(), token->column(), msg);
    }
    
    void Reporter::reportError(const Error& err)
    {
        if(err.m_token)
        {
            reportError(err.m_token, err.what());
        }
    }
    
    std::string Reporter::error() const
    {
        if(m_useColor)
        {
            return "\033[1;31merror:\033[0m";
        }
        
        return "error:";
     }
     
     std::string Reporter::indicator() const
     {
        if(m_useColor)
        {
            return "\033[1;32m^\033[0m";
        }
        
        return "^";
     }
}