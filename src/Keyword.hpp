/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_KEYWORD_HPP
#define BODHI_KEYWORD_HPP

#include "TokenType.hpp"

#include <string>
#include <array>

namespace bodhi
{
    // TODO: const accessors
    class Keyword
    {
    public:
        std::string     name;
        TokenType       type;
        
        static const std::array<Keyword, 19>& getKeywords();
    };
}

#endif