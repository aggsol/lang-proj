/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_INTERPRETER_HPP
#define BODHI_INTERPRETER_HPP

#include "Expression.hpp"
#include "Statement.hpp"
#include "Token.hpp"
#include "Value.hpp"

#include <unordered_map>

namespace bodhi
{
    class Reporter;
    
    class Interpreter : public IExpressionVisitor, public IStatementVisitor
    {
    public:
        explicit Interpreter(Reporter& reporter);
    
        ~Interpreter() {}
    
        const Value& interpret(Expression::Ptr expr);
        
        void interpret(std::deque<Statement::Ptr>& statements, std::unordered_map<Expression::Ptr, int>& locals);
    
        // statements
        void visitBlock(Block& stmt) override;
        void visitType(TypeStatement& stmt) override;
        void visitExpressionStatement(ExpressionStatement& stmt) override;
        void visitProcedure(Procedure& stmt) override;
    
        // expressions
        void visitBinary(Binary& expr) override;
        void visitUnary(Unary& expr) override;
        void visitLiteral(Literal& expr) override;
        void visitGrouping(Grouping& expr) override;
    
    private:
        void evaluate(Expression& expr);
        void evaluate(Statement& stmt);
        void evaluate(std::deque<Statement::Ptr>& statements);
        
        void checkNumberOperand(Token::Ptr token, const Value& val);
        void checkNumberOperands(Token::Ptr token, const Value& left,
            const Value& right);
    
        Reporter&   m_reporter;
        Value       m_value;
    };
}

#endif