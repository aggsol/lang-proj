/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "TokenType.hpp"

#include "CliArg.hpp"
#include "Interpreter.hpp"
#include "Parser.hpp"
#include "Reporter.hpp"
#include "Tokenizer.hpp"
#include "Version.hpp"

#include <fstream>
#include <sys/stat.h>

namespace
{
    void printUsage()
    {
        std::cout << "Usage:\n"
            << "  main                      REPL mode\n"
            << "  main <FILE>               Execute file\n"
            << "Options:\n"
            << "  -e, --eval <ARG>          Evaluate the given argument\n"
            << "  -n, --no-color            Disable ANSI color output\n"
            << "  -h, --help                Print this help\n"
            << "      --version             Print version"
            << std::endl;
    }
    
    void evalLine(std::shared_ptr<std::string> line, bool useColor)
    {
        bodhi::Reporter reporter(useColor);
        bodhi::Tokenizer tokenizer(line, 1, reporter);
        
        auto tokens = tokenizer.scanTokens();
        
        if(tokenizer.hasError()) 
        {
            std::cerr << "Tokenizer has errors\n";
            return;
        }
        
        //std::cout << "num tokens=" << tokens.size() << "\n";
        
        bodhi::Parser parser(tokens, reporter);
        auto ast = parser.parse();
        
        if(parser.hasError()) 
        { 
            std::cerr << "Parser has errors\n";
            return;
        }
        
        bodhi::Interpreter interpreter(reporter);
        auto result = interpreter.interpret(ast);
        //std::cout << "isBool=" << result.isBool() << " bool=" << result.getBool() << "\n";
        std::cout << result << "\n";
    }
    
    void runRepl(bool useColor)
    {
        auto line = std::make_shared<std::string>();
        while (true)
        {
            std::cout << "> ";
        
            std::getline(std::cin, *line);
            if(line->compare("exit") == 0 || line->compare("quit") == 0)
            {
                break;
            }
            
            evalLine(line, useColor);
        }
    }
    
    int runFile(const std::string& filename, bool useColor)
    {
        struct stat buf;
        if(stat(filename.c_str(), &buf) != 0)
        {
            std::cerr << "Cannot open file: " << filename << std::endl;
            return 101;
        }
        
        if(S_ISREG(buf.st_mode) == 0)
        {
            std::cerr << "Cannot open file: " << filename << std::endl;
            return 102;
        }
        
        bodhi::Reporter reporter(useColor);
        reporter.setFilename(filename);
        
        std::ifstream ifs(filename);
        auto line = std::make_shared<std::string>();
        
        unsigned lineNum = 0;
        while(std::getline(ifs, *line))
        {
            bodhi::Tokenizer tokenizer(line, lineNum, reporter);

            std::cout << *line << std::endl;
            auto tokens = tokenizer.scanTokens();
            std::cout << "num tokens=" << tokens.size() << " ";
            for(auto& t : tokens)
            {
                std::cout << t->type() << " ";
            }
            std::cout << std::endl;
            
            lineNum++;
            // create a new line as we keep it for error messages.
            // unused lines are released here
            line = std::make_shared<std::string>();
            
            if(tokenizer.hasError())
            {
                return 103;
            }
            
            bodhi::Parser parser(tokens, reporter);
            auto ast = parser.parse();
        }
        
        return 0;
    }
}

int main(int argc, char *argv[])
{
    bodhi::CliArg< bool > help(argc, argv, "h", "help");
    bodhi::CliArg< bool > version(argc, argv, "", "version");
    bodhi::CliArg< bool > noColor(argc, argv, "n", "--no-color");
    
    if(help)
    {
        printUsage();
        return 0;
    }
    
    if(version)
    {
        std::cout << langproj_VERSION_MAJOR << "."
            << langproj_VERSION_MINOR << "."
            << langproj_VERSION_PATCH << "\n";
        return 0;
    }
    
    try
    {
        if(argc == 2)
        {
            return runFile(argv[1], !noColor);
        }
        else
        {
            runRepl(!noColor);
        }
    }
    catch(const std::exception& ex)
    {
        std::err << "Uncatched exception: " << ex.what() << std::endl;
        return 101;
    }
    
    return 0;
}