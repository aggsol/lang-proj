/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "Value.hpp"

#include <iostream>
#include <cmath>

namespace bodhi
{
    Value::Value()
    : m_tag(Tag::Nil)
    {}

    Value::Value(const char* str)
    : Value(std::string(str))
    {}
    
    Value::Value(const std::string& value)
    : m_tag(Tag::String)
    , m_string(value)
    {}

    Value::Value(bool value)
    {
        if(value)
        {
            m_tag = Tag::True;
        }
        else
        {
            m_tag = Tag::False;
        }
    }
    
    Value::Value(double value)
    : m_tag(Tag::Number)
    , m_number(value)
    { }
    
    bool Value::isString() const noexcept
    {
        return m_tag == Tag::String;
    }
    
    bool Value::isNumber() const noexcept
    {
        return m_tag == Tag::Number;
    }
    
    bool Value::isBool() const noexcept
    {
        return (m_tag == Tag::True) || (m_tag == Tag::False);
    }
    
    void Value::setNil()
    {
        m_tag = Tag::Nil;
    }
    
    Value& Value::operator=(const double& val)
    {
        m_tag = Tag::Number;
        m_number = val;
        return *this;
    }
    
    Value& Value::operator=(const std::string& val)
    {
        m_tag = Tag::String;
        m_string = val;
        return *this;
    }
    
    Value& Value::operator=(const bool& val)
    {
        if(val) 
        {
            m_tag = Tag::True;    
        }
        else
        {
            m_tag = Tag::False;
        }
        return *this;
    }
 
    bool Value::operator==(const Value& other) const
    {
        if(this->isNil() && other.isNil()) return true;
        if(this->isNil()) return false;
        if(this->m_tag != other.m_tag) return false;
        
        const bool isLeftNum = this->isNumber();
        const bool isRightNum = other.isNumber();
        if(isLeftNum && isRightNum)
        {
            return this->m_number == other.m_number;
        }
        
        const bool isLeftString = this->isString();
        const bool isRightString = other.isString();
        if(isLeftString && isRightString)
        {
            return this->m_string == other.m_string;
        }
     
        if(this->isBool() && other.isBool())
        {
            return this->m_tag == other.m_tag;
        }
        
        return false;
    }
 
    bool Value::isTrue() const noexcept
    {
        if(m_tag == Tag::Nil) return false;
        if(m_tag == Tag::False) return false;
        
        return true;
    }
 
    bool Value::operator!=(const Value& other) const
    {
        return not(*this == other);
    }
 
    std::ostream& operator<<(std::ostream& os, const Value& val)
    {
        switch(val.m_tag)
        {
        case Value::Tag::Nil:
            os << "nil";
        break;
        case Value::Tag::Number:
            os << val.m_number;
        break;
        case Value::Tag::String:
            os << val.m_string;
        break;
        case Value::Tag::True:
            os << "true";
        break;
        case Value::Tag::False:
            os << "false";
        break;
        }

        return os;
    }   
}