/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "Resolver.hpp"

#include <cassert>

namespace bodhi {
 
void Resolver::resolveLocal(Expression::Ptr, Token& token)
{
    assert(m_scopes.size() > 0);
    for(int i= m_scopes.size() - 1; i>=0; --i)
    {
        auto& currScope = m_scopes[i];
        auto it = currScope.find(token.lexeme());
        if(it != currScope.end())
        {
            
        }
    }
}

}