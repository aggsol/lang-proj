/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_REPORTER_HPP
#define BODHI_REPORTER_HPP

#include "Token.hpp"

#include <iostream>

namespace bodhi
{
    class Error;
    
    class Reporter
    {
    public:
        explicit Reporter(bool useColor);
        Reporter(bool useColor, std::ostream& strm);
    
        void reportError(const std::string& line, int lineNum, int col, 
            const std::string& msg);
    
        void reportError(Token::Ptr token, const std::string& msg);
        void reportError(const Error& err);
    
        void setFilename(const std::string& file) { m_filename = file; } 
    
    private:
    
        std::string indicator() const;
        std::string error() const;
    
        std::ostream&   m_output;
        bool            m_useColor = false;
        std::string     m_filename;
    };
}

#endif