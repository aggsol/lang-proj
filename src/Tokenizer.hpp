/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_TOKENIZER_HPP
#define BODHI_TOKENIZER_HPP

#include "Token.hpp"

#include <string>
#include <deque>

class TokenizerTest;

namespace bodhi
{
    class Reporter;
    
    class Tokenizer
    {
    public:
        Tokenizer(std::shared_ptr<std::string> line, unsigned lineNum, Reporter& reporter);
    
        std::deque<Token::Ptr>&& scanTokens();
        
        bool                        hasError() const { return m_hasError; }
    
    private:
        friend class ::TokenizerTest;
        std::deque<Token::Ptr>&& scanTokens(std::shared_ptr<std::string> line, unsigned lineNum);
    
        bool                        isEnd() const;
        bool                        match(char c);
        
        char                        advance();
        char                        current() const;
        char                        next() const;
    
        void                        handleIdentifier();
        void                        handleNumber();
        void                        handleString();
        void                        appendToken(TokenType type);
    
        std::deque<Token::Ptr>          m_tokens;
        std::shared_ptr<std::string>    m_line;
        unsigned                        m_lineNum = 0;
        unsigned                        m_current = 0;
        unsigned                        m_tokenStart = 0;
        
        Reporter&                       m_reporter;
        bool                            m_hasError = false;
    };
}

#endif