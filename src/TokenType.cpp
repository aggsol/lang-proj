#include "TokenType.hpp"

#include <array>
#include <string>

namespace bodhi
{
    namespace
    {
        std::array<std::string, 42> s_name = {
            {"left bracket"
            ,"right bracket"
            ,"comma"
            ,"dot"
            ,"plus"
            ,"minus"
            ,"slash"
            ,"asterisk"
            ,"hash"
            
            ,"bang"
            ,"bang-equal"
            ,"equal"
            ,"equal-equal"
            ,"greater"
            ,"greater-equal"
            ,"less"
            ,"less-equal"

            ,"identifier"
            ,"string"
            ,"number"
            ,"true"
            ,"false"
            ,"and"
            ,"or"
            ,"not"
            ,"nil"
            ,"procedure"
            ,"return"
            ,"type"
            ,"self"
            ,"extends"
            ,"mother"
            ,"for"
            ,"to"
            ,"step"
            ,"if"
            ,"while"
            ,"end"
            ,"variable"
            , "newline"
            ,"final"}
        };
    }

    std::string convertToString(TokenType type)
    {
        return s_name.at(static_cast<int>(type));    
    }
    
    std::ostream& operator<<(std::ostream& os, const TokenType& fr)
    {
        os <<  s_name.at(static_cast<int>(fr));
        return os;
    }

}