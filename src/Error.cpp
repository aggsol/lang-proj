/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "Error.hpp"

namespace bodhi
{
    Error::Error(int lineNum, int col, const std::string& line, const std::string& msg)
    : std::runtime_error(msg)
    , m_lineNum(lineNum)
    , m_column(col)
    , m_line(line)
    , m_token(nullptr)
    {
        
    }
    
    Error::Error(Token::Ptr token, const std::string& msg)
    : std::runtime_error(msg)
    , m_line()
    , m_token(token)
    {
        
    }
}