/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_TOKEN_TYPE_HPP
#define BODHI_TOKEN_TYPE_HPP

#include <string>
#include <iostream>

namespace bodhi
{
    enum class TokenType
    {
        LeftBracket, RightBracket,
        Comma, Dot,
        Plus, Minus, Slash, Asterisk,
        Hash,
        
        Bang, BangEqual,
        Equal, EqualEqual,
        Greater, GreaterEqual,
        Less, LessEqual,
        
        Identifier, String, Number,
        True, False,
        And, Or, Not,
        Nil, Procedure, Return,
        Type, Self, Extends, Mother,
        For, To, Step,
        If, While, End,
        Variable,
        
        Newline,
        Final
    };
    
    std::string convertToString(TokenType type);
    
    std::ostream& operator<<(std::ostream& os, const TokenType& fr);
}

#endif