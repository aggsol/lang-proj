/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_ENVIRONMENT_HPP
#define BODHI_ENVIRONMENT_HPP

#include "Value.hpp"

#include <iostream>
#include <map>
#include <string>

namespace bodhi
{
    class Token;
    
    class Environment : public std::enable_shared_from_this<Environment>
    {
    public:
        typedef std::shared_ptr<Environment> Ptr;
    
        Environment();
        explicit Environment(Environment::Ptr enclosing);
    
    private:
        void declare(const Token& token);
        void assign(const Token& token, Value::Ptr value);
        void assginAt(int distance, const Token& token, Value::Ptr value);
        void define(const std::string& name, Value::Ptr value);
        
        Environment::Ptr enterScope();
        
        Value::Ptr getAt(int distance, const std::string& name);
        Value::Ptr get(Token& token);
    
        std::map<std::string, Value::Ptr>   m_values;
        Environment::Ptr                    m_enclosing;
    };
    
    std::ostream& operator<<(std::ostream& os, const Environment& fr);
}

#endif