/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "Interpreter.hpp"
#include "Error.hpp"
#include "Reporter.hpp"

#include <cassert>
#include <sstream>

namespace bodhi
{
    Interpreter::Interpreter(Reporter& reporter)
    : m_reporter(reporter)
    , m_value()
    {
        
    }
    
    void Interpreter::visitGrouping(Grouping& expr)
    {
        evaluate(*expr.m_expression);
    }
    
    void Interpreter::visitLiteral(Literal& expr)
    {
        m_value = expr.value();
    }
    
    void Interpreter::visitAssignment(Assignment& expr)
    {
    
    }
    
    void Interpreter::visitBinary(Binary& expr)
    {
        assert(expr.m_operator);
        auto op = expr.m_operator->type();
        
        assert(expr.m_left);
        evaluate(*expr.m_left);
        auto left = m_value;
        
        assert(expr.m_right);
        evaluate(*expr.m_right);
        auto right = m_value;
        
        const bool isLeftString = left.isString();
        const bool isRightString = right.isString();
        
        switch(op)
        {
            case TokenType::Minus:
                checkNumberOperands(expr.m_operator, left, right);
                m_value = (left.getNumber() - right.getNumber());
            break;
            case TokenType::Plus:
                
                if(left.isNumber() && right.isNumber())
                {
                    m_value = (left.getNumber() + right.getNumber());
                }
                else if(isLeftString && isRightString)
                {
                    m_value = left.getString() + right.getString();
                }
                else
                {
                    throw Error(expr.m_operator, "Operands for (+) must be two numbers or two strings.");
                }
            break;
            case TokenType::Slash:
                checkNumberOperands(expr.m_operator, left, right);
                if(right.getNumber() == 0)
                {
                    throw Error(expr.m_operator, "Division by zero.");
                }
                m_value = (left.getNumber() / right.getNumber());
            break;
            case TokenType::Asterisk:
                checkNumberOperands(expr.m_operator, left, right);
                m_value = (left.getNumber() * right.getNumber());
            break;
            case TokenType::Greater:
                checkNumberOperands(expr.m_operator, left, right);
                m_value = (left.getNumber() > right.getNumber());
            break;
            case TokenType::GreaterEqual:
                checkNumberOperands(expr.m_operator, left, right);
                m_value = (left.getNumber() >= right.getNumber());
            break;
            case TokenType::Less:
                checkNumberOperands(expr.m_operator, left, right);
                m_value = (left.getNumber() < right.getNumber());
            break;
            case TokenType::LessEqual:
                checkNumberOperands(expr.m_operator, left, right);
                m_value = (left.getNumber() <= right.getNumber());
            break;
            case TokenType::EqualEqual:
                m_value = (left == right);
            break;
            case TokenType::BangEqual:
                m_value = (left != right);            
            break;
            default:
                    std::ostringstream msg;
                    msg << "Cannot " << op
                        << " left=" << left 
                        << " right=" << right;
                    throw Error(expr.m_operator, msg.str());
            break;
        }
    }
    
    void Interpreter::visitUnary(Unary& expr)
    {
        assert(expr.m_right);
        evaluate(*expr.m_right);
        const auto& right = m_value;
        
        if(expr.m_operator->type() == TokenType::Minus)
        {
            checkNumberOperand(expr.m_operator, right);
            m_value = -right.getNumber();
            return;
        }
        else if(expr.m_operator->type() == TokenType::Not)
        {
           m_value = !m_value.isTrue();
           return;
        }
        else
        {
            throw Error(expr.m_operator, "Invalid unary operator");
        }
        
        m_value.setNil();
    }
    
    void Interpreter::evaluate(Expression& expr)
    {
        m_value.setNil();
        expr.accept(*this);
    }
    
    void Interpreter::evaluate(Statement& stmt)
    {
        stmt.accept(*this);
    }
    
    void Interpreter::evaluate(std::deque<Statement::Ptr>& statements)
    {
        for(auto s : statements)
        {
            evaluate(*s);
        }
    }
    
    void Interpreter::checkNumberOperand(Token::Ptr token, const Value& val)
    {
         if(val.isNumber()) return;
         throw Error(token, "Operand must be a number.");
    }
    
    void Interpreter::checkNumberOperands(Token::Ptr token, const Value& left,
        const Value& right)
    {
         if(left.isNumber() && right.isNumber()) return;
         throw Error(token, "Both operands must be a numbers.");
    }
    
    const Value& Interpreter::interpret(Expression::Ptr expr)
    {
        try
        {
            evaluate(*expr);
        }
        catch(const Error& ex)
        {
            m_reporter.reportError(ex);
            m_value.setNil();
        }
        return m_value;
    }
    
    void Interpreter::visitBlock(Block& stmt)
    {

    }
    
    void Interpreter::visitType(TypeStatement& stmt)
    {
        
    }
    
    void Interpreter::visitExpressionStatement(ExpressionStatement& stmt)
    {
        
    }
    
    void Interpreter::visitProcedure(Procedure& stmt)
    {
        
    }
}