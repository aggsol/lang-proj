/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_VALUE_HPP
#define BODHI_VALUE_HPP

#include <limits>
#include <memory>
#include <string>

namespace bodhi
{
    class Value
    {
    public:
        typedef std::shared_ptr<Value> Ptr;
        
        Value();
        explicit Value(const char* str);
        explicit Value(const std::string& value);
        explicit Value(bool value);
        explicit Value(double value);

        void setNil();

        double              getNumber() const noexcept { return m_number; }
        bool                getBool() const noexcept { return m_tag == Tag::True; }
        const std::string&  getString() const noexcept { return m_string; }

        Value& operator=(const double& val);
        Value& operator=(const std::string& val);
        Value& operator=(const bool& val);

        bool operator==(const Value& other) const;
        bool operator!=(const Value& other) const;

        bool isNil() const noexcept { return m_tag == Tag::Nil; }
        bool isString() const noexcept;
        bool isNumber() const noexcept;
        bool isBool() const noexcept;
        bool isTrue() const noexcept;
    
    private:
        
        enum class Tag
        {
            Nil,
            True,
            False,
            Number,
            String
        };
        
        Tag         m_tag = Tag::Nil;
        std::string m_string = "<nil>";
        double      m_number = std::numeric_limits<double>::quiet_NaN();
        
        friend std::ostream& operator<<(std::ostream& os, const Value& val);
    };
    
    std::ostream& operator<<(std::ostream& os, const Value& val);
}

#endif