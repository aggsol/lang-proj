/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_PARSER_HPP_
#define BODHI_PARSER_HPP_

#include "Expression.hpp"
#include "Token.hpp"

#include <initializer_list>
#include <deque>

namespace bodhi
{
class Reporter;
class Token;

class Parser
{
public:
    explicit Parser(std::deque<Token::Ptr>& tokens, Reporter& reporter);

    Expression::Ptr     parse();
    bool hasError() const { return m_hasError; }

private:

    Expression::Ptr     expression();
    Expression::Ptr     equality();
    Expression::Ptr     comparison();
    Expression::Ptr     term();
    Expression::Ptr     factor();
    Expression::Ptr     unary();
    Expression::Ptr     primary();

    bool                match(std::initializer_list<TokenType> types);
    bool                check(TokenType type);
    bool                isAtEnd();

    Token::Ptr          advance();
    Token::Ptr          peek();
    Token::Ptr          previous();
    Token::Ptr          consume(TokenType, const std::string&);

    void                synchronize();

    std::deque<Token::Ptr>  m_tokens;
    int                     m_current = 0;
    bool                    m_hasError = false;
    Reporter&               m_reporter;
};
}

#endif