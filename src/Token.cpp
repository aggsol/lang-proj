/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "Token.hpp"

#include <cassert>

namespace bodhi
{
    const std::string& Token::lexeme()
    {
        if(m_line != nullptr && m_lexeme.empty()
            && m_type != TokenType::Newline
            && m_type != TokenType::Final)
        {
            m_lexeme = ( m_line->substr(m_column, m_length) );
        }
        return m_lexeme;
    }
    
    std::string Token::line() const
    {
        if(m_line != nullptr)
        {
            return *m_line;
        }
        return "<Invalid Line>";
    }
}