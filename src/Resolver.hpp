/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_RESOLVER_HPP
#define BODHI_RESOLVER_HPP

#include "Expression.hpp"
#include "Statement.hpp"

#include <map>
#include <stack>

namespace bodhi
{

    class Resolver : public IExpressionVisitor, public IStatementVisitor
    {
    public:
    
        // statements
        void visitBlock(Block& stmt) override;
        void visitType(TypeStatement& stmt) override;
        void visitExpressionStatement(ExpressionStatement& stmt) override;
        void visitProcedure(Procedure& stmt) override;
        
        // expressions
        void visitBinary(Binary& expr) override;
        void visitUnary(Unary& expr) override;
        void visitLiteral(Literal& expr) override;
        void visitGrouping(Grouping& expr) override;
    
    private:
    
        enum class ProcedureType
        {
            None = 0,
            Procedure,
            Method,
            Initilizer
        };
    
        enum class DataType
        {
            None = 0,
            CompisiteType,
            ExtendedType
        };
    
        void    resolveLocal(Expression::Ptr, Token& token);
    
        std::deque< std::map<std::string, bool> >   m_scopes;
        std::map<Expression::Ptr, int>              m_locals;
    
        ProcedureType                               m_currentProcedure = ProcedureType::None;
        DataType                                    m_currentDataType = DataType::None;
    };
}

#endif