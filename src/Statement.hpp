/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#ifndef BODHI_STATEMENT_HPP
#define BODHI_STATEMENT_HPP

#include <deque>
#include <memory>

namespace bodhi
{
    class Block;
    class TypeStatement;
    class ExpressionStatement;
    class Procedure;
    
    class IStatementVisitor
    {
    public:
        virtual void visitBlock(Block& stmt) = 0;
        virtual void visitType(TypeStatement& stmt) = 0;
        virtual void visitExpressionStatement(ExpressionStatement& stmt) = 0;
        virtual void visitProcedure(Procedure& stmt) = 0;
        
    protected:
        virtual ~IStatementVisitor() {}
    };
    
    class Statement
    {
    public:
        typedef std::shared_ptr<Statement> Ptr;
    
        virtual void accept(IStatementVisitor& visitor) = 0;
    
    protected:
        virtual ~Statement() {}
    };
    
    class Block : public Statement
    {
    public:
        Block(const std::deque<Statement::Ptr>& statements)
        : m_statements(statements)
        {
            
        }
        
        void accept(IStatementVisitor& visitor) override
        {
            visitor.visitBlock(*this);
        }
        
        std::deque<Statement::Ptr>  m_statements;
    };
}

#endif