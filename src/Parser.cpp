/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "Parser.hpp"

#include "Reporter.hpp"
#include "TokenType.hpp"

#include <memory>
#include <stdexcept>

namespace bodhi
{
    Parser::Parser(std::deque<Token::Ptr>& tokens, Reporter& reporter)
    : m_tokens(std::move(tokens))
    , m_reporter(reporter)
    {
        
    }
    
    Expression::Ptr Parser::expression()
    {
        return equality();
    }
    
    Expression::Ptr Parser::equality()
    {
        auto expr = comparison();
        
        auto types = {TokenType::BangEqual, TokenType::EqualEqual};
        while(match(types))
        {
            Token::Ptr op = previous();
            Expression::Ptr right = comparison();
            expr = std::make_shared<Binary>(expr, op, right);
        }
        
        return expr;
    }
    
    Expression::Ptr Parser::comparison()
    {
        auto expr = term();
        
        auto types = {TokenType::Greater, TokenType::GreaterEqual,
            TokenType::Less, TokenType::LessEqual };
        
        while(match(types))
        {
            auto op = previous();
            auto right = term();
            expr = std::make_shared<Binary>(expr, op, right);
        }
        
        return expr;
    }
    
    Expression::Ptr Parser::term()
    {
        auto expr = factor();
        
        auto types = {TokenType::Minus, TokenType::Plus};
        while(match(types))
        {
            auto op = previous();
            auto right = factor();
            expr = std::make_shared<Binary>(expr, op, right);
        }
        
        return expr;
    }
    
    Expression::Ptr Parser::factor()
    {
        auto expr = unary();
        auto types = {TokenType::Slash, TokenType::Asterisk};
        
        while(match(types))
        {
            auto op = previous();
            auto right = unary();
            expr = std::make_shared<Binary>(expr, op, right);
        }
        
        return expr;
    }
    
    Expression::Ptr Parser::unary()
    {
        auto types = {TokenType::Minus, TokenType::Not};
        
        if(match(types))
        {
            auto op = previous();
            auto right = unary();
            return std::make_shared<Unary>(op, right);
        }
        
        return primary();
    }
    
    Expression::Ptr Parser::primary()
    {
        if(match({TokenType::False}))
        {
            return std::make_shared<Literal>(false);
        }
        if(match({TokenType::True}))
        {
            return std::make_shared<Literal>(true);
        }
        if(match({TokenType::Nil}))
        {
            return std::make_shared<Literal>();
        }
        if(match({TokenType::String}))
        {
            // the lexeme contains "
            assert(previous()->lexeme().length() >= 2);
            auto end = previous()->lexeme().length()-2;
            auto value = previous()->lexeme().substr(1, end);
            return std::make_shared<Literal>(value);
        }
        if(match({TokenType::Number}))
        {
            double value = std::stod(previous()->lexeme(), nullptr);
            return std::make_shared<Literal>(value);
        }
        if(match({TokenType::LeftBracket}))
        {
            auto expr = expression();
            consume(TokenType::RightBracket, "Expecting ')' after expression. ");
            
            return std::make_shared<Grouping>(expr);
        }
        
        m_reporter.reportError(peek(), "Expecting expression");
        m_hasError = true;
        
        return nullptr;
    }
    
    bool Parser::match(std::initializer_list<TokenType> types)
    {
        for(auto t: types)
        {
            if(check(t))
            {
                advance();
                return true;
            }
        }
        
        return false;
    }
    
    bool Parser::check(TokenType type)
    {
        if(isAtEnd())
        {
            return false;
        }
        return peek()->type() == type;
    }
    
    bool Parser::isAtEnd()
    {
        return peek()->type() == TokenType::Final;
    }
    
    Token::Ptr Parser::advance()
    {
        if(not isAtEnd()) 
        {
            m_current++;
        }
        return previous();
    }
    
    Token::Ptr Parser::peek()
    {
        return m_tokens.at(m_current);
    }
    
    Token::Ptr Parser::previous()
    {
        return m_tokens.at(m_current-1);
    }
 
    Token::Ptr Parser::consume(TokenType type, const std::string& msg)
    {
        if(check(type))
        {
            return advance();    
        }
        m_hasError = true;
        m_reporter.reportError(peek(), msg);
        return nullptr;
    }
    
    void Parser::synchronize()
    {
        advance();
        while(not isAtEnd())
        {
            if(previous()->type() == TokenType::Newline) return;
            
            switch(peek()->type())
            {
                case TokenType::Type:
                case TokenType::Procedure:
                case TokenType::Variable:
                case TokenType::For:
                case TokenType::If:
                case TokenType::While:
                case TokenType::Return:
                return;
                
                default:
                break;
            }
            
            advance();
        }
    }
    
    Expression::Ptr Parser::parse()
    {
        return expression();
    }
}