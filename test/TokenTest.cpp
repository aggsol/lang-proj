/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "tiny-unit.hpp"

#include "../src/TokenType.hpp"

#include <iostream>
#include <sstream>

class TokenTest : public tiny::Unit
{
public:
    TokenTest()
    : tiny::Unit("TokenTest")
    {
        tiny::Unit::registerTest(&TokenTest::stream, "stream");
        tiny::Unit::registerTest(&TokenTest::namesOrder, "namesOrder");
    }
    
    static void stream()
    {
        std::ostringstream buffer;
        buffer << bodhi::TokenType::Slash;
        
        TINY_ASSERT_EQUAL(buffer.str(), "slash");
    }
    
    static void namesOrder()
    {
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::Mother), "mother");
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::Asterisk), "asterisk");
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::Procedure), "procedure");
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::Plus), "plus");
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::Self), "self");
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::Minus), "minus");
        
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::If), "if");
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::While), "while");

        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::Newline), "newline");
        TINY_ASSERT_EQUAL(convertToString(bodhi::TokenType::Final), "final");
    }

};

TokenTest tokenTest;