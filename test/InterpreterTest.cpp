/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "tiny-unit.hpp"

#include "../src/Interpreter.hpp"
#include "../src/Reporter.hpp"
#include "../src/Tokenizer.hpp"
#include "../src/Parser.hpp"

class InterpreterTest : public tiny::Unit
{
public:
    InterpreterTest()
    : tiny::Unit("InterpreterTest")
    {
        tiny::Unit::registerTest(&InterpreterTest::weirdNotEqual, "weirdNotEqual");
        tiny::Unit::registerTest(&InterpreterTest::concatStrings, "concatStrings");
        tiny::Unit::registerTest(&InterpreterTest::boolComparison, "boolComparison");
        tiny::Unit::registerTest(&InterpreterTest::calculate, "calculate");
    }
    
    static void weirdNotEqual()
    {
        std::ostringstream  stream;
        bodhi::Reporter     reporter(false, stream);
        auto input = std::make_shared<std::string>("true != (12 > 12)");
        bodhi::Tokenizer tokenizer(input, 1, reporter);
        auto tokens = tokenizer.scanTokens();
        
        bodhi::Parser parser(tokens, reporter);
        auto ast = parser.parse();
        
        TINY_ASSERT_OK(not parser.hasError());
        TINY_ASSERT_OK(ast != nullptr);
        
        bodhi::Interpreter interpreter(reporter);
        auto result = interpreter.interpret(ast);
        
        TINY_ASSERT_OK(result.isBool());
        TINY_ASSERT_EQUAL(result.getBool(), true);
    }
    
    static void concatStrings()
    {
        std::ostringstream  stream;
        bodhi::Reporter     reporter(false, stream);
        auto input = std::make_shared<std::string>("\"Hello \" +  \"World!\"");
        bodhi::Tokenizer tokenizer(input, 1, reporter);
        auto tokens = tokenizer.scanTokens();
        
        bodhi::Parser parser(tokens, reporter);
        auto ast = parser.parse();
        
        TINY_ASSERT_OK(ast != nullptr);
        
        bodhi::Interpreter interpreter(reporter);
        auto result = interpreter.interpret(ast);
        
        TINY_ASSERT_OK(result.isString());
        TINY_ASSERT_EQUAL(result.getString(), "Hello World!");
    }   
    
    static void boolComparison()
    {
        std::ostringstream  stream;
        bodhi::Reporter     reporter(false, stream);
        auto input = std::make_shared<std::string>("(1 <2) != (2<1)");
        bodhi::Tokenizer tokenizer(input, 1, reporter);
        auto tokens = tokenizer.scanTokens();
        
        bodhi::Parser parser(tokens, reporter);
        auto ast = parser.parse();
        
        TINY_ASSERT_OK(ast != nullptr);
        
        bodhi::Interpreter interpreter(reporter);
        auto result = interpreter.interpret(ast);
        
        TINY_ASSERT_OK(result.isBool());
        TINY_ASSERT_EQUAL(result.getBool(), true);
    }    
    
    static void calculate()
    {
        std::ostringstream  stream;
        bodhi::Reporter     reporter(false, stream);
        auto input = std::make_shared<std::string>("1*2*(3+4)-1");
        bodhi::Tokenizer tokenizer(input, 1, reporter);
        auto tokens = tokenizer.scanTokens();
        
        bodhi::Parser parser(tokens, reporter);
        auto ast = parser.parse();
        
        TINY_ASSERT_OK(ast != nullptr);
        
        bodhi::Interpreter interpreter(reporter);
        auto result = interpreter.interpret(ast);
        
        TINY_ASSERT_OK(result.isNumber());
        TINY_ASSERT_EQUAL(result.getNumber(), 13);
    }
};

InterpreterTest interpreterTest;