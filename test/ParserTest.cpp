/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "tiny-unit.hpp"

#include "../src/Reporter.hpp"
#include "../src/Tokenizer.hpp"
#include "../src/Parser.hpp"

class ParserTest : public tiny::Unit
{
public:
    ParserTest()
    : tiny::Unit("ParserTest")
    {
        tiny::Unit::registerTest(&ParserTest::groupings, "groupings");
        tiny::Unit::registerTest(&ParserTest::parserError, "parserError");
        tiny::Unit::registerTest(&ParserTest::parserWithTokenizer, "parserWithTokenizer");
    }
    
    static void groupings()
    {
        std::ostringstream buffer;
        bodhi::Reporter reporter(false, buffer);
        auto input = std::make_shared<std::string>("(((1+2)))+1");
        bodhi::Tokenizer tokenizer(input, 1, reporter);
        
        auto tokens = tokenizer.scanTokens();
        
        TINY_ASSERT_OK(not tokenizer.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 12);
        
        bodhi::Parser parser(tokens, reporter);
        
        auto expr = parser.parse();
        
        //std::cout << "reporter: " << buffer.str() << "\n";
        
        TINY_ASSERT_OK(not parser.hasError());
    }
    
    static void parserError()
    {
        std::stringstream buffer;
        bodhi::Reporter reporter(false, buffer);
        auto input = std::make_shared<std::string>("ik=1\n1>=false");
        bodhi::Tokenizer tokenizer(input, 1, reporter);
        
        auto tokens = tokenizer.scanTokens();
        bodhi::Parser parser(tokens, reporter);
        
        TINY_ASSERT_OK(not tokenizer.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 0);
        
        auto expr = parser.parse();
        
        //std::cout << buffer.str();
        
        TINY_ASSERT_OK(expr == nullptr);
    }
    
    static void parserWithTokenizer()
    {
        std::stringstream buffer;
        bodhi::Reporter reporter(false, buffer);
        auto input = std::make_shared<std::string>("1/1\n1/1");
        bodhi::Tokenizer tokenizer(input, 1, reporter);
        
        auto tokens = tokenizer.scanTokens();
        bodhi::Parser parser(tokens, reporter);
        
        TINY_ASSERT_EQUAL(tokens.size(), 0);
        
        auto expr = parser.parse();
        
        TINY_ASSERT_OK(expr != nullptr);
    }
};

ParserTest parserTest;