/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "tiny-unit.hpp"

#include "../src/Tokenizer.hpp"
#include "../src/Reporter.hpp"

#include <sstream>

class TokenizerTest : public tiny::Unit
{
    public:
    TokenizerTest()
    : tiny::Unit("TokenizerTest")
    {
        tiny::Unit::registerTest(&TokenizerTest::brackets, "brackets");
        tiny::Unit::registerTest(&TokenizerTest::strings, "strings");
        tiny::Unit::registerTest(&TokenizerTest::errors, "errors");
        tiny::Unit::registerTest(&TokenizerTest::multiToken, "multiToken");
        tiny::Unit::registerTest(&TokenizerTest::singleToken, "singleToken");
        tiny::Unit::registerTest(&TokenizerTest::scanLine, "scanLine");
    }

    static void brackets()
    {
        std::ostringstream  stream;
        bodhi::Reporter     reporter(false, stream);
        auto input = std::make_shared<std::string>("(((1+2)))+3");
        bodhi::Tokenizer tokenizer(input, 1, reporter);
        auto tokens = tokenizer.scanTokens();
        
        TINY_ASSERT_OK(not tokenizer.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 12);
    }

    static void strings()
    {
        std::ostringstream  stream;
        bodhi::Reporter     reporter(false, stream);
        auto input = std::make_shared<std::string>("echo \"Hello world\"");
        bodhi::Tokenizer tokenizer(input, 1, reporter);
        auto tokens = tokenizer.scanTokens();
        
        TINY_ASSERT_OK(not tokenizer.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 3);
        TINY_ASSERT_EQUAL(tokens[0]->type(), bodhi::TokenType::Identifier);
        TINY_ASSERT_EQUAL(tokens[1]->type(), bodhi::TokenType::String);
        TINY_ASSERT_EQUAL(tokens[1]->lexeme(), "\"Hello world\"");
        TINY_ASSERT_EQUAL(tokens[2]->type(), bodhi::TokenType::Newline);
        
        input = std::make_shared<std::string>("\"\"");
        tokens = tokenizer.scanTokens(input, 2);

        TINY_ASSERT_OK(not tokenizer.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 2);
        TINY_ASSERT_EQUAL(tokens[0]->type(), bodhi::TokenType::String);
        TINY_ASSERT_EQUAL(tokens[0]->lexeme(), "\"\"");
        
        input = std::make_shared<std::string>("var i= \"");
        tokens = tokenizer.scanTokens(input, 3);

        TINY_ASSERT_OK(tokenizer.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 4);
    }

    static void errors()
    {
        std::ostringstream  stream;
        bodhi::Reporter     reporter(false, stream);
        auto input = std::make_shared<std::string>("aaa == 2bb2"); // TODO what shoud this parse to?!
        bodhi::Tokenizer tokenizer(input, 1, reporter);
        auto tokens = tokenizer.scanTokens();
        
        // for(auto& t : tokens)
        // {
        //     std::cout << t->type() << "\n";
        // }
        
        TINY_ASSERT_EQUAL(tokens.size(), 4);
    }

    static void multiToken()
    {
        std::ostringstream  stream;
        bodhi::Reporter     reporter(false, stream);
        auto input = std::make_shared<std::string>("=\t<\t>\t!=\t<=\t>=");
        bodhi::Tokenizer tokenizer(input, 1, reporter);
        auto tokens = tokenizer.scanTokens();
        
        TINY_ASSERT_OK(not tokenizer.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 7);
        TINY_ASSERT_EQUAL(tokens[0]->type(), bodhi::TokenType::Equal);
        TINY_ASSERT_EQUAL(tokens[0]->lexeme(), "=");
        
        TINY_ASSERT_EQUAL(tokens[1]->type(), bodhi::TokenType::Less);
        TINY_ASSERT_EQUAL(tokens[2]->type(), bodhi::TokenType::Greater);
        TINY_ASSERT_EQUAL(tokens[3]->type(), bodhi::TokenType::BangEqual);
        
        TINY_ASSERT_EQUAL(tokens[4]->type(), bodhi::TokenType::LessEqual);
        TINY_ASSERT_EQUAL(tokens[5]->type(), bodhi::TokenType::GreaterEqual);
        TINY_ASSERT_EQUAL(tokens[6]->type(), bodhi::TokenType::Newline);
    }
    
    static void singleToken()
    {
        std::ostringstream  stream;
        bodhi::Reporter     reporter(false, stream);
        auto input = std::make_shared<std::string>("( . , + - / * )");
        bodhi::Tokenizer tokenizer(input, 1, reporter);
        auto tokens = tokenizer.scanTokens();
        
        TINY_ASSERT_OK(not tokenizer.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 9);
        TINY_ASSERT_EQUAL(tokens[0]->type(), bodhi::TokenType::LeftBracket);
        TINY_ASSERT_EQUAL(tokens[1]->type(), bodhi::TokenType::Dot);
        TINY_ASSERT_EQUAL(tokens[2]->type(), bodhi::TokenType::Comma);
        TINY_ASSERT_EQUAL(tokens[3]->type(), bodhi::TokenType::Plus);
        
        TINY_ASSERT_EQUAL(tokens[4]->type(), bodhi::TokenType::Minus);
        TINY_ASSERT_EQUAL(tokens[5]->type(), bodhi::TokenType::Slash);
        TINY_ASSERT_EQUAL(tokens[6]->type(), bodhi::TokenType::Asterisk);
        TINY_ASSERT_EQUAL(tokens[7]->type(), bodhi::TokenType::RightBracket);
    }
    
    static void scanLine()
    {
        std::ostringstream  stream;
        bodhi::Reporter     reporter(false, stream);

        auto input = std::make_shared<std::string>("var i=12.3\n\r\t");
        bodhi::Tokenizer tokenizer(input, 1, reporter);
        auto tokens = tokenizer.scanTokens();

        TINY_ASSERT_OK(not tokenizer.hasError());
        TINY_ASSERT_EQUAL(tokens.size(), 6);
        TINY_ASSERT_EQUAL(tokens[0]->type(), bodhi::TokenType::Variable);
        
        std::string lexeme = tokens[0]->lexeme();
        TINY_ASSERT_EQUAL(lexeme, "var");
        
        TINY_ASSERT_EQUAL(tokens[1]->type(), bodhi::TokenType::Identifier);
        TINY_ASSERT_EQUAL(tokens[1]->lexeme(), "i");
        
        TINY_ASSERT_EQUAL(tokens[2]->type(), bodhi::TokenType::Equal);
        
        TINY_ASSERT_EQUAL(tokens[3]->type(), bodhi::TokenType::Number);
        TINY_ASSERT_EQUAL(tokens[3]->lexeme(), "12.3");
        
        TINY_ASSERT_EQUAL(tokens[4]->type(), bodhi::TokenType::Newline);
        TINY_ASSERT_EQUAL(tokens[5]->type(), bodhi::TokenType::Newline);
    }
};

TokenizerTest tokenizerTest;