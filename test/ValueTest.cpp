/*
 Copyright (C) 2017  Kim HOANG
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
*/
#include "tiny-unit.hpp"

#include "../src/Value.hpp"

#include <string>

class ValueTest : public tiny::Unit
{
public:
    ValueTest()
    : tiny::Unit("ValueTest")
    {
        tiny::Unit::registerTest(&ValueTest::stream, "stream");
        tiny::Unit::registerTest(&ValueTest::testBase, "testBase");
    }
    
    static void stream()
    {
        bodhi::Value value(true);
        TINY_ASSERT_OK(value.isTrue());
        
        std::ostringstream strm;
        strm << value;
        
        
        TINY_ASSERT_EQUAL(strm.str(), "true");
        
        bodhi::Value other(1.33);
        
        TINY_ASSERT_OK(value != other);
        
        value = other;
        TINY_ASSERT_OK(value.isTrue());
        TINY_ASSERT_EQUAL(value, other);
        
        strm.str("");
        strm << value;
        TINY_ASSERT_EQUAL(strm.str(), "1.33");
    }
    
    static void testBase()
    {
        bodhi::Value value(true);
        
        TINY_ASSERT_OK(not value.isNil());
        
        value.setNil();
        
        TINY_ASSERT_OK(value.isNil());
        
        value = 3.1337;
        
        bool isNumber = value.isNumber();
        
        TINY_ASSERT_OK(isNumber);
        TINY_ASSERT_EQUAL(value.getNumber(), 3.1337);
        
        value = false;
        
        bool isBool = value.isBool();
        
        TINY_ASSERT_OK(isBool);
        TINY_ASSERT_EQUAL(value.getBool(), false);
        
        value = std::string("foobar");
        
        bool isString = value.isString();
        
        TINY_ASSERT_OK(isString);
        TINY_ASSERT_EQUAL(value.getString(), "foobar");
    }
};

ValueTest valueTest;